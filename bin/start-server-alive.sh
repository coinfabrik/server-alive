#!/usr/bin/env bash

pkill --signal SIGINT -f 'start-server-monitor'

node /more-blockchains/server-alive/bin/start-server-monitor