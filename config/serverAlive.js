var commandLineArgs = require('command-line-args');
var path = require('path');
var winston = require('winston');

var config = {
  verbose: true,
  // ethServers: ["52.40.138.237", "52.36.145.169", "api.jaxx.io"],
  // ethServers: ["api.jaxx.io"],
  etcServers: ["52.40.138.237:8080", "52.36.145.169:8080"],
  // rskOldServers: ["54.67.34.99"],
  // rskServers: ["52.36.145.169:7000", "52.40.138.237:7000", "api.jaxx.io:7000"],
  rskServers: ["52.36.145.169:7000"],
  // rskServers: ["52.36.145.169:7000", "api.jaxx.io"],
  bitcoinInsightServers: ["52.40.138.237:2082/insight-api/status", "52.36.145.169:2082/insight-api/status", "api.jaxx.io:2082/insight-api/status"],
  dashInsightServers: ["52.40.138.237:2052/insight-api-dash/status", "52.36.145.169:2052/insight-api-dash/status", "api.jaxx.io:2052/insight-api-dash/status"],
  dashServers: ["52.36.145.169:3011/api/dash/blockchainInfo"],
  zcashServers: ["52.40.138.237:3006/api/zec/blockchaininfo", "52.36.145.169:3006/api/zec/blockchaininfo", "api.jaxx.io/api/zec/blockchaininfo"],
  ltcServers: ["52.40.138.237:3000/api/ltc/blockchaininfo", "52.36.145.169:3000/api/ltc/blockchaininfo", "api.jaxx.io/api/ltc/blockchaininfo"],
  // ltcServers: ["52.36.145.169:3000/api/ltc/blockchaininfo", "api.jaxx.io/api/ltc/blockchaininfo"],
  dogeServers: ["52.40.138.237:3001/api/doge/blockchainInfo", "52.36.145.169:3001/api/doge/blockchaininfo", "api.jaxx.io/api/doge/blockchaininfo"],  
  digiServers: ["52.40.138.237:3005/api/dgb/blockchaininfo", "52.36.145.169:3005/api/dgb/blockchaininfo", "api.jaxx.io/api/dgb/blockchaininfo"],
  startServers: ["52.40.138.237:3007/api/start/blockchaininfo", "52.36.145.169:3007/api/start/blockchaininfo", "api.jaxx.io/api/start/blockchaininfo"],
  emerServers: ["52.36.145.169:3002/api/emc/blockchaininfo", "api.jaxx.io/api/emc/blockchaininfo"],
  reddServers: ["52.40.138.237:3004/api/rdd/blockchaininfo", "52.36.145.169:3004/api/rdd/blockchaininfo", "api.jaxx.io/api/rdd/blockchaininfo"],
  notificationEmails: ['pablo.yabo@coinfabrik.com', 'pablo.yabo@gmail.com'],
  writeToStdOut: true,
  onlyCheck: false,
  logFile: (process.platform == 'win32' ? process.env.APPDATA + '\\jaxx\\back-end\\server-verifier.log' : '/var/log/jaxx/server-verifier.log'),
  errorReporter: {
    ip: '',
    port: 9999
  }
};

var error;
var help;

// config.ethServers = ["localhost:8089", "52.40.138.237", "52.36.145.169", "api.jaxx.io"];

// -t <time period>: the script will scan again after <time period>
// -o <filename>: output goes to <filename>
// -v : verbose mode on
// -c : only check connections but it doesn't notify Slack or by email
var usage = "usage: serverAlive [-o <filename>] [-t <time period>'] [-v or -verbose] [-c or -check] [-s or -stdout]";

var args = [
  {name: 'time', alias: 't', type: String},
  {name: 'verbose', alias: 'v', type: Boolean},
  {name: 'check', alias: 'c', type: Boolean},
  {name: 'output', alias: 'o', type: String},
  {name: 'stdout', alias: 's', type: Boolean}
];

var options = commandLineArgs(args);

options.output ? config.logFile = options.output
  : (process.platform == 'win32' ? process.env.APPDATA + '\\jaxx\\back-end\\server-verifier.log' : '/var/log/jaxx/server-verifier.log');

options.time ? config.timeout = parseInt(options.time)
  : config.timeout = undefined;

// config.verbose = options.verbose || false;
config.verbose = true;

options.stdout ? config.writeToStdOut = true
  : config.writeToStdOut = false;

config.logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)(),
    new (winston.transports.File)({filename: config.logFile}),
  ]
});

if (config.writeToStdOut)
  config.logger.add(winston.transports.Console);

if (error) {
  console.log("Error: " + error);
  console.log(usage);
  process.exit(0);
}
else if (help) {
  console.log(usage);
  process.exit(0);
}

module.exports = config;
