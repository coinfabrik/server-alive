var express = require('express');
var router = express.Router();
var serverAliveStatus = require('../serverAlive').coins();
var _ = require('lodash');

/* GET home page. */
router.get('/', function(req, res, next) {
  var ccoins = [];
  serverAliveStatus.forEach((coin) => {
    var c = {};
    c.servers = [];
    coin.servers.forEach((server) => {
      var s = {};
      s.name = coin.name;
      s.ip = server.server;
      s.status = (server.errors.length === 0);
      s.lastCheck = server.lastCheckEnd;
      s.deltaCheck = (server.lastCheckEnd - server.lastCheckStart > 0) ? server.lastCheckEnd - server.lastCheckStart : 'checking...';
      s.lastBlock = server.latestMaxBlockInfo.maxBlockNumberInserted;
      c.servers.push(s);
    });

    ccoins.push(c);
  });

  res.render('status', { title: 'Server Status', coins: ccoins});
});

module.exports = router;
