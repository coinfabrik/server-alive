var http = require('http');
var https = require('https');
var path = require('path');

var _ = require('lodash');
var request = require('request');
var Slack = require('slack-node');
var async = require('async');
var forever = require('forever-monitor');

//apiToken = "xoxp-2440864763-18324328800-65435386595-a46d0b859c";
var apiToken = "xoxb-96206451922-I0tyqfCW0weUv8HgaEebY9v8";
var slack = new Slack(apiToken);

var config = require('./config/serverAlive');

var MIN_TIME_BETWEEN_RESTARTS = 30000; // ms
var MAX_BLOCK_DIFF_WARNING = 30;
var MAX_BLOCK_DIFF_ERROR = 50;
var SLACK_NOTIFICATION_TIMEOUT_MINUTES = 10;
var EMAIL_NOTIFICATION_TIMEOUT_MINUTES = 60;
var MAXIMUM_BLOCK_ALLOWED_DISTANCE_ETHEREUM = 30;
var MAXIMUM_BLOCK_ALLOWED_DISTANCE_ROOTSTOCK = 30;
var MAXIMUM_BLOCK_ALLOWED_DISTANCE_BITCOIN = 60;
var MAXIMUM_BLOCK_ALLOWED_DISTANCE_ZCASH = 25;
var MAXIMUM_BLOCK_ALLOWED_DISTANCE_DASH = 25;
var MAXIMUM_BLOCK_ALLOWED_DISTANCE_LTC = 25;
var MAXIMUM_BLOCK_ALLOWED_DISTANCE_DOGE = 60;
var MAXIMUM_BLOCK_ALLOWED_DISTANCE_DIGI = 60;
var MAXIMUM_BLOCK_ALLOWED_DISTANCE_START = 60;
var MAXIMUM_BLOCK_ALLOWED_DISTANCE_EMER = 60;
var MAXIMUM_BLOCK_ALLOWED_DISTANCE_REDD = 60;
var MAXIMUM_RESTART_COUNT = 10;
var CHECK_INTERVAL = 5000;
var VALID_RESTART_INTERVAL = 15000;

var error;
var help;
var localIP = 'localhost';
var latestNotification;
var latestErrorCount = 0;
var latestWarningCount = 0;
var latestMessageIP;
var latestMessageTime;
var errorInfo;
var changeDetectedCount = 0;
var oldMessages = [];

var editDistance = (s1, s2) => {
  s1 = s1.toLowerCase();
  s2 = s2.toLowerCase();

  var costs = new Array();
  for (var i = 0; i <= s1.length; i++) {
    var lastValue = i;
    for (var j = 0; j <= s2.length; j++) {
      if (i == 0)
        costs[j] = j;
      else {
        if (j > 0) {
          var newValue = costs[j - 1];
          if (s1.charAt(i - 1) != s2.charAt(j - 1))
            newValue = Math.min(Math.min(newValue, lastValue),
                costs[j]) + 1;
          costs[j - 1] = lastValue;
          lastValue = newValue;
        }
      }
    }
    if (i > 0)
      costs[s2.length] = lastValue;
  }
  return costs[s2.length];
}

var similarity = (s1, s2) => {
  var longer = s1;
  var shorter = s2;
  if (typeof s1 !== 'undefined' && typeof s2 !== 'undefined') {
    if (s1.length < s2.length) {
      longer = s2;
      shorter = s1;
    }
    var longerLength = longer.length;
    if (longerLength == 0) {
      return 1.0;
    }
    return (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength);
  } else {
    return 0
  }
}

var fetchIP = () => {
  return new Promise((resolve, reject) => {
    try {
      request('http://169.254.169.254/latest/meta-data/public-ipv4', (err, resp, body) => {
        if (err) {
          config.logger.error('Error reaching Amazon public IP checker');
          resolve();
        } else {
          localIP = body;
          config.errorReporter.ip = body;
          resolve();
        }
      })
    } catch (err) {
      config.logger.error('Error reaching Amazon public IP checker');
      resolve();
    }
  });
};

var buildServerInfo = (servers) => {
  return _.reduce(servers, (result, server) => {
    result.push({
      server: server,
      errors: [],
      pastErrors: [],
      restartableErrors: [],
      lastCheckStart: null,
      lastCheckEnd: null,
      restartCount: 0,
      restartDate: null,
      latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 }
    });
    return result;
  }, []);
};

var checkErrors = (coinInfo, servers, errors, warnings, timeoutMinutes) => {
  var currentTime = new Date().getTime();

  for (var j = 0; j < servers.length; j++) {
    var errMsg;
    var typeErr;

    if (!_.isEqual(servers[j].errors, [])) {
      servers[j].errors.forEach((err) => {
        errors.push(generateError(coinInfo.name, servers[j].server, err));
      })
    }
    // report if the last block didn't change in the last minutes
    else if (currentTime - servers[j].latestMaxBlockInfo.timestamp > 1000 * 60 * timeoutMinutes) {
      errMsg = 'Latest synced block ' + servers[j].latestMaxBlockInfo.maxBlockNumberInserted +
        " didn't change in the last " + timeoutMinutes + " minutes";
      errors.push(generateError(coinInfo.name, servers[j].server, errMsg));
    }
    else if (servers[j].latestMaxBlockInfo.maxBlockNumberInserted < coinInfo.latestMaxBlockInfo.maxBlockNumberInserted - MAX_BLOCK_DIFF_WARNING) {
      errMsg = 'Latest synced block for ' + servers[j].server +
        ' is ' + (coinInfo.latestMaxBlockInfo.maxBlockNumberInserted - servers[j].latestMaxBlockInfo.maxBlockNumberInserted) + ' blocks behind the blockchain';

      if (servers[j].latestMaxBlockInfo.maxBlockNumberInserted < coinInfo.latestMaxBlockInfo.maxBlockNumberInserted - MAX_BLOCK_DIFF_ERROR) {
        errors.push(generateError(coinInfo.name, servers[j].server, errMsg));
      }
      else {
        typeErr = "WARNING";
        warnings.push(generateError(coinInfo.name, servers[j].server, errMsg, typeErr));
      }
    } else {
      errMsg = '';
    }
    serverErrorOutput(coinInfo.name, servers[j].server, errMsg, typeErr);
    servers[j].errors = [];
    servers[j].pastErrors = [];
    servers[j].restartableErrors = [];
  }
};

var coinsToVerify = [
  /* {
    name: "Ethereum",
    type: 'ether',
    apiDir: 'eth',
    servers: buildServerInfo(config.ethServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_ETHEREUM,
    restartScript: '/srv/ethereum_full_node/bin/start-eth-jaxx-back-end.sh',
    faultTolerance: 3,
    restartCount: 0,
  },
  {
    name: "Rootstock",
    type: 'ether',
    apiDir: 'eth',
    servers: buildServerInfo(config.rskOldServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_ROOTSTOCK,
    protocol: 'http',
    restartScript: '/srv/ethereum_full_node/bin/start-rsk-jaxx-back-end.sh',
    faultTolerance: 3,
    restartCount: 0,
  }, */
  {
    name: "Rootstock",
    type: 'ether',
    apiDir: 'sbtc',
    servers: buildServerInfo(config.rskServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_ROOTSTOCK,
    restartScript: '/srv/ethereum_full_node/bin/start-rsk-jaxx-back-end.sh',
    faultTolerance: 3,
    restartCount: 0,
  },
  {
    name: "Ethereum Classic",
    type: 'ether',
    apiDir: 'eth',
    servers: buildServerInfo(config.etcServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_ETHEREUM,
    restartScript: '/srv/ethereum_full_node/bin/start-etc-jaxx-back-end.sh',
    faultTolerance: 3,
    restartCount: 0,
  },
  {
    name: "Bitcoin",
    type: 'insight',
    servers: buildServerInfo(config.bitcoinInsightServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_BITCOIN,
    faultTolerance: 3,
    restartCount: 0,
  },
  {
    name: "Dash",
    type: 'bitclone',
    servers: buildServerInfo(config.dashServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_DASH,
    restartScript: '/more-blockchains/dash/start-coin-node.sh',
    faultTolerance: 3,
    restartCount: 0,
  },
  {
    name: "Zcash",
    type: 'bitclone',
    servers: buildServerInfo(config.zcashServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_ZCASH,
    restartScript: '/more-blockchains/zcash/run-backend.sh',
    faultTolerance: 3,
    restartCount: 0,
  },
  {
    name: "Litecoin",
    type: 'bitclone',
    servers: buildServerInfo(config.ltcServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_LTC,
    restartScript: '/more-blockchains/litecoin/run-backend.sh',
    faultTolerance: 3,
    restartCount: 0,
  },
  {
    name: "Dogecoin",
    type: 'bitclone',
    servers: buildServerInfo(config.dogeServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_DOGE,
    restartScript: '/more-blockchains/dogecoin/run-backend.sh',
    faultTolerance: 3,
    restartCount: 0,
  },
  {
    name: "Digibyte",
    type: 'bitclone',
    servers: buildServerInfo(config.digiServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_DIGI,
    restartScript: '/more-blockchains/digibyte/run-backend.sh',
    faultTolerance: 3,
    restartCount: 0,
  },
  {
    name: "Startcoin",
    type: 'bitclone',
    servers: buildServerInfo(config.startServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_START,
    restartScript: '/more-blockchains/startcoin/run-backend.sh',
    faultTolerance: 3,
    restartCount: 0,
  },
  {
    name: "Emercoin",
    type: 'bitclone',
    servers: buildServerInfo(config.emerServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_EMER,
    restartScript: '/more-blockchains/emercoin/run-backend.sh',
    faultTolerance: 3,
    restartCount: 0,
  },
  {
    name: "Reddcoin",
    type: 'bitclone',
    servers: buildServerInfo(config.reddServers),
    latestMaxBlockInfo: { maxBlockNumberInserted: 0, timestamp: 0 },
    MAXIMUM_BLOCK_ALLOWED_DISTANCE: MAXIMUM_BLOCK_ALLOWED_DISTANCE_REDD,
    restartScript: '/more-blockchains/reddcoin/run-backend.sh',
    faultTolerance: 3,
    restartCount: 0,
  }
];

var verifyServers = () => {
  errorInfo = [];
  if (config.verbose) {
    config.logger.info('\nChecking servers ...');
  }

  var errors = [];
  var warnings = [];

  async.eachSeries(coinsToVerify, (coinInfo, callback) => {
    verifyServerArray(coinInfo, coinInfo.servers, 0, (servers) => {
      checkErrors(coinInfo, servers, errors, warnings, coinInfo.MAXIMUM_BLOCK_ALLOWED_DISTANCE);
      callback(null);
    });
  }, (err) => {
    reportIncidents(errors, warnings);

    setTimeout(verifyServers, CHECK_INTERVAL);
  });
};

var verifyServerArray = (coinInfo, servers, idx, callback) => {
  if (idx >= servers.length) {
    callback(servers);
    return;
  }

  servers[idx].lastCheckStart = new Date();

  var protocol = http;
  var port = '80';
  var server = servers[idx].server;
  if (servers[idx].server.indexOf('api.jaxx') !== -1 && (coinInfo.type === 'bitclone' || coinInfo.apiDir === 'sbtc')) {
    protocol = https;
    port = '443';
  }

  var portIndex = servers[idx].server.indexOf(':');
  var rpath = '';

  if (coinInfo.type === 'ether') {
    var apiDir = coinInfo.apiDir || 'eth';
    port = (protocol == http ? '80' : '443');
    if (portIndex != -1) {
      port = servers[idx].server.substr(portIndex + 1);
      server = servers[idx].server.substr(0, portIndex);
    }
    rpath = '/api/' + apiDir + '/latestBlockNumberInserted';
  } else if (coinInfo.type === 'insight') {
    if (portIndex != -1) {
      var portEnd = servers[idx].server.indexOf('/', portIndex + 1);
      port = servers[idx].server.substr(portIndex + 1, portEnd - portIndex - 1);
      rpath = servers[idx].server.substr(portEnd);
      server = servers[idx].server.substr(0, portIndex);
    }
  } else if (coinInfo.type === 'bitclone') {
    if (portIndex != -1) {
      var portEnd = servers[idx].server.indexOf('/', portIndex + 1);
      port = servers[idx].server.substr(portIndex + 1, portEnd - portIndex - 1);
      rpath = servers[idx].server.substr(portEnd);
      server = servers[idx].server.substr(0, portIndex);
    }
    else {
      var serverEnd = servers[idx].server.indexOf('/', 1);
      rpath = servers[idx].server.substr(serverEnd);
      server = servers[idx].server.substr(0, serverEnd);
    }
  }

  var options = {
    timeout: 60000,
    host: server,
    port: port,
    path: rpath,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      'Accept': "application/json",
      'Cache-Control': "no-cache"
    }
  };

  var req = protocol.request(options, (res) => {
    var msg = '';

    res.on('data', (chunk) => {
      msg += chunk;
    });

    req.on('error', (err) => {
      config.logger.error('Failed on protocol request ' + err);
      servers[idx].errors.push(err);
      if (err.indexOf('ECONNRESET') !== -1 || err.indexOf('ETIMEOUT') !== -1 || err.indexOf('ECONNREFUSED') !== -1) {
        servers[idx].restartableErrors.push(err);
      }
    });

    res.on('end', () => {
      try {
        msg = JSON.parse(msg);
        if (coinInfo.type === 'ether') {
          if (!msg.latestBlockNumberInserted) {
            servers[idx].errors.push('Cannot get latestBlockNumberInserted')
          }
          else {
            if (msg.latestBlockNumberInserted > coinInfo.latestMaxBlockInfo.maxBlockNumberInserted) {
              coinInfo.latestMaxBlockInfo.maxBlockNumberInserted = msg.latestBlockNumberInserted;
              coinInfo.latestMaxBlockInfo.timestamp = new Date().getTime();
            }

            servers[idx].latestMaxBlockInfo.maxBlockNumberInserted = msg.latestBlockNumberInserted;
            servers[idx].latestMaxBlockInfo.timestamp = new Date().getTime();

            serverErrorOutput(coinInfo.name, servers[idx].server, 'last block ' + msg.latestBlockNumberInserted, "INFO");
          }
        } else if (coinInfo.type === 'insight') {
          if (!msg.info || !msg.info.blocks) {
            servers[idx].errors.push('Cannot get latestBlockNumberInserted');
          }
          else {
            if (msg.info.blocks > coinInfo.latestMaxBlockInfo.maxBlockNumberInserted) {
              coinInfo.latestMaxBlockInfo.maxBlockNumberInserted = msg.info.blocks;
              coinInfo.latestMaxBlockInfo.timestamp = new Date().getTime();
            }
            servers[idx].latestMaxBlockInfo.maxBlockNumberInserted = msg.info.blocks;
            servers[idx].latestMaxBlockInfo.timestamp = new Date().getTime();

            serverErrorOutput(coinInfo.name, servers[idx].server, 'last block ' + msg.info.blocks, "INFO");
          }
        } else if (coinInfo.type === 'bitclone') {
          if (!msg.last_synched_block) {
            servers[idx].errors.push('Cannot get blockchaininfo');
          }
          else {
            if (msg.last_synched_block > coinInfo.latestMaxBlockInfo.maxBlockNumberInserted) {
              coinInfo.latestMaxBlockInfo.maxBlockNumberInserted = msg.last_synched_block;
              coinInfo.latestMaxBlockInfo.timestamp = new Date().getTime();
            }
            servers[idx].latestMaxBlockInfo.maxBlockNumberInserted = msg.last_synched_block;
            servers[idx].latestMaxBlockInfo.timestamp = new Date().getTime();

            serverErrorOutput(coinInfo.name, servers[idx].server, 'last block ' + msg.last_synched_block, "INFO");
          }
        }
      } catch (err) {
        config.logger.error('Failure - res on end event error: ' + err + ', ' + msg);
        servers[idx].errors.push(err + ', ' + msg);
        serverErrorOutput(coinInfo.name, servers[idx].server, servers[idx].errors);
        if (msg.indexOf('ECONNRESET') !== -1 || msg.indexOf('ETIMEOUT') !== -1 || msg.indexOf('ECONNREFUSED') !== -1 || msg.indexOf('bad gateway error') !== -1) {
          servers[idx].restartableErrors.push(err + ', ' + msg);
        }
      }

      if (!_.isEqual(servers[idx].errors, []) && !_.isEqual(servers[idx].errors, servers[idx].pastErrors)) {
        if (servers[idx].errors.length < coinInfo.faultTolerance) {
          servers[idx].pastErrors = servers[idx].errors;
          servers[idx].lastCheckEnd = new Date();
          verifyServerArray(coinInfo, servers, idx, callback);
          return;
        }
      }

      servers[idx].lastCheckEnd = new Date();
      verifyServerArray(coinInfo, servers, idx + 1, callback);
    });
  });

  req.on('error', (err) => {
    servers[idx].errors.push(err.message);
    config.logger.error('Failure - req error event: ' + JSON.stringify(err));
    if (err.code.indexOf('ECONNRESET') !== -1
      || err.code.indexOf('ETIMEOUT') !== -1
      || err.code.indexOf('ECONNREFUSED') !== -1
      || err.code.indexOf('EADDRNOTAVAIL') !== -1) {
      servers[idx].restartableErrors.push(err);
    }
    if (servers[idx].errors.length < coinInfo.faultTolerance) {
      servers[idx].lastCheckEnd = new Date();
      verifyServerArray(coinInfo, servers, idx + 1, callback);
      return;
    } else {
      if (servers[idx].server.indexOf(localIP) !== -1 && servers[idx].restartableErrors.length < MAXIMUM_RESTART_COUNT) {
        if (!config.onlyCheck) {
          /* slack.api('chat.postMessage', {
           text: 'Running server restart script for ' + coinInfo.name + '\r\n' +
                 'Errors: ' + JSON.stringify(servers[idx].restartableErrors) ,
           channel: '#jaxx-back-end-issues'
           }, (err, response) => { }); */
        }

        if ((new Date().valueOf() - servers[idx].restartDate) > MIN_TIME_BETWEEN_RESTARTS
          && VALID_RESTART_INTERVAL > (new Date().valueOf() - servers[idx].lastCheckEnd.valueOf())) {
          config.logger.warn('Running server restart script for ' + coinInfo.name + '\r\n' +
            'Errors: ' + JSON.stringify(servers[idx].restartableErrors));
          servers[idx].restartDate = new Date().valueOf();
          config.logger.warn(servers[idx].restartDate);
          var child = forever.start([coinInfo.restartScript], {
            silent: true,
            minUptime: 30000
          });
          child.on('error', (err) => {
            config.logger.error(err);
          });
          child.start();
          coinInfo.restartCount += 1;
          servers[idx].errors = [];
          servers[idx].pastErrors = [];
          servers[idx].restartableErrors = [];
          }
        }
    }
    serverErrorOutput(coinInfo.name, servers[idx].server, servers[idx].errors);
    if (config.verbose) {
      config.logger.error('req.on error');
      config.logger.info(servers[idx].server + ': ERROR ' + servers[idx].errors);
    }

    servers[idx].lastCheckEnd = new Date();
    verifyServerArray(coinInfo, servers, idx + 1, callback);
  });

  req.end();
};

var validateReport = (error) => {
  var similarMessages = _.reduce(oldMessages, (result, message) => {
    if (similarity(message.text, error) > 0.5) {
      result.push(message);
      return result;
    } else {
      return result;
    }
  }, []);

  if (typeof similarMessages === 'undefined' || _.isEqual(similarMessages, [])) {
    return true;
  } else {
    var serverIp = error.split(' ')[1].split(':')[0];
    var validMessages = _.filter(similarMessages, (message) => {
      if(message.text.indexOf(serverIp) !== -1) {
        return message;
      }
    });

    if (typeof validMessages[0] === 'undefined') {
      return true;
    }

    var reporterIp = validMessages[0].text.split('\n')[0].split('-')[0];
    var reportTime = parseFloat(validMessages[0].timestamp);

    var validResolutionMessagesFromIp = _.reduce(oldMessages, (result, message) => {
      if (message.text.indexOf(reporterIp) !== -1
        && message.text.indexOf('Issues are now fixed') !== -1
        && parseFloat(message.timestamp) > reportTime) {
        result.push(message);
        return result;
      } else {
        return result;
      }
    }, []);

    if (validResolutionMessagesFromIp.length === 0) {
      return true;
    } else {
      return false;
    }
  }
};

var notifyIfNeeded = (errors, warnings) => {
  if (errors.length == 0 && warnings.length == 0 && latestErrorCount == 0 && latestWarningCount == 0) {
    changeDetectedCount = 0;
    return;
  }

  var changed = (errors.length != latestErrorCount || warnings.length != latestWarningCount);
  var currentTime = new Date().getTime();

  if (latestMessageTime && latestMessageIP) {
    // if the last message was generated by this node just wait until SLACK_NOTIFICATION_TIMEOUT_MINUTES. Otherwise, wait another 2/10 of that time
    // to give some time to the old notification master notify again
    if (currentTime - latestMessageTime < 1000 * 60 * SLACK_NOTIFICATION_TIMEOUT_MINUTES) {
      return;
    }
    else if (localIP != latestMessageIP) {
      if (currentTime - latestMessageTime < 1000 * 60 * (SLACK_NOTIFICATION_TIMEOUT_MINUTES + SLACK_NOTIFICATION_TIMEOUT_MINUTES / 5)) {
        return;
      }
    }
  }

  //config.logger.info('changeDetectedCount ' + changeDetectedCount);

  // var the master report first, after 3 times report it
  if (!changed) {
    changeDetectedCount = 0;
    return;
  }

  if(localIP != latestMessageIP && changeDetectedCount <= 4 || changeDetectedCount <= 2) {
    changeDetectedCount++;
    return;
  }

  //config.logger.info('Reached changeDetectedCount ' + changeDetectedCount);
  changeDetectedCount = 0;

  var errCount = 0;
  var warnCount = 0;
  var reportString = '';
  var subject;

  for (var i = 0; i < errors.length; i++) {
    if (reportString.indexOf(errors[i]) === -1 && validateReport(errors[i])) {
      reportString += 'ERROR: ' + errors[i] + '\n';
      errCount += 1;
    }
  }

  for (var i = 0; i < warnings.length; i++) {
    if (reportString.indexOf(warnings[i]) === -1 && validateReport(warnings[i])) {
      reportString += 'WARNING: ' + warnings[i] + '\n';
      warnCount += 1;
    }
  }

  latestErrorCount = errCount;
  latestWarningCount = warnCount;

  reportString = localIP + '-' + errCount + '-' + warnCount + '\n' + reportString;

  config.logger.warn('Server incidents: \n' + reportString);

  // there was an issue and it is fixed now
  if (errCount == 0 && warnCount == 0) {
    reportString += 'Issues are now fixed';
    subject = 'jaxx-back-end Issues Solved';
    coinsToVerify.forEach((coin) => {
      coin.restartCount = 0;
    });
  }
  else {
    subject = 'jaxx-back-end Error';
  }


  if (!config.onlyCheck) {
    /* slack.api('chat.postMessage', {
     text: reportString,
     channel: '#jaxx-back-end-issues'
     }, (err, response) => {
       latestErrorCount = errCount;
       latestWarningCount = warnCount;
     }); */

    if (latestErrorCount == errCount && latestWarningCount == warnCount) {
      // don't notify more one time each hour
      if (latestNotification && (currentTime - latestNotification) < 1000 * 60 * EMAIL_NOTIFICATION_TIMEOUT_MINUTES) {
        return;
      }
    }
  }

  config.logger.warn('Server incidents: \n' + reportString);
};

var retrieveSlackLastMessageData = (callback) => {
  slack.api('groups.history', {
    channel: "G1YU472GP",
    count: 20
  }, (err, response) => {
    if (!err && response.messages.length > 0) {
      var pos = response.messages[0].text.indexOf('-');
      if (pos != -1) {
        latestMessageIP = response.messages[0].text.substr(0, pos);
        var end = response.messages[0].text.indexOf('-', ++pos);
        if (end != -1) {
          latestErrorCount = parseInt(response.messages[0].text.substr(pos, end - pos));
          pos = end + 1;
          end = response.messages[0].text.indexOf('\n', pos);
          if (end != -1) {
            latestWarningCount = parseInt(response.messages[0].text.substr(pos, end - pos));
          }
        }
      }

      latestMessageTime = new Date(response.messages[0].ts * 1000).getTime();
    }
    oldMessages = _.reduce(response.messages, (result, message) => {
      result.push({text: message.text, timestamp: message.ts});
      return result;
    }, []);
    callback();
  });
};

var reportIncidents = (errors, warnings) => {
  //config.logger.info('Errors ' + errors.length);
  retrieveSlackLastMessageData(() => {
    //config.logger.info('Last Error ' + latestErrorCount);
    if (errors.length == 0 && warnings.length == 0 && latestErrorCount == 0 && latestWarningCount == 0) {
      changeDetectedCount = 0;
      return;
    }

    var latestMessageTime;
    var latestMessageIP;

    notifyIfNeeded(errors, warnings);
  });

};

var generateError = (coinName, server, errorStr, type) => {
  return coinName + ' ' + server + (type ? (': ' + type + ' ') : ': ERROR ') + errorStr;
};

var serverErrorOutput = (coinName, server, error, type) => {
  if (error && config.verbose) {
    config.logger.info(generateError(coinName, server, error, type));
  }
};

var verifyIPAddress = () => {

  if (config.verbose) {
    config.logger.info('IP: ' + localIP);
  }
  retrieveSlackLastMessageData(() => {
    setTimeout(verifyServers, 1000);
  });
};

var run = () => {
  // start the server-alive process and start the error reporting process
  fetchIP().then(() => {
    setTimeout(verifyIPAddress, 1000);
  }).catch(() => {
    config.logger.error('Couldn\'t reach Amazon IP checker');
    setTimeout(verifyIPAddress, 1000);
  })
};

var getCoinsToVerify = () => {
  return coinsToVerify;
};

module.exports = {
  run: run,
  coins: getCoinsToVerify,
};
